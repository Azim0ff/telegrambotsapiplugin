package icu.azim.telegrambots;

import net.byteflux.libby.Library;
import net.byteflux.libby.LibraryManager;

public class TelegramBotsAPIPlugin{
	
	public static void loadLibs(LibraryManager manager) {
		manager.addMavenCentral();
		manager.loadLibrary(buildLibrary("org.telegram", "telegrambots", "4.5"));
		manager.loadLibrary(buildLibrary("com.fasterxml.jackson.core", "jackson-annotations", "2.10.1"));
		manager.loadLibrary(buildLibrary("com.fasterxml.jackson.jaxrs", "jackson-jaxrs-json-provider", "2.10.1"));
		manager.loadLibrary(buildLibrary("com.fasterxml.jackson.jaxrs", "jackson-jaxrs-base", "2.10.1"));
		manager.loadLibrary(buildLibrary("com.fasterxml.jackson.module", "jackson-module-jaxb-annotations", "2.10.1"));
		manager.loadLibrary(buildLibrary("com.fasterxml.jackson.core", "jackson-core", "2.10.1"));
		manager.loadLibrary(buildLibrary("jakarta.xml.bind", "jakarta.xml.bind-api", "2.3.2"));
		manager.loadLibrary(buildLibrary("jakarta.activation", "jakarta.activation-api", "1.2.1"));
		manager.loadLibrary(buildLibrary("com.fasterxml.jackson.core", "jackson-databind", "2.10.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.jersey.inject", "jersey-hk2", "2.29.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.jersey.core", "jersey-common", "2.29.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.hk2", "osgi-resource-locator", "1.0.3"));
		manager.loadLibrary(buildLibrary("org.glassfish.hk2", "hk2-locator", "2.6.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.hk2.external", "aopalliance-repackaged", "2.6.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.hk2", "hk2-api", "2.6.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.hk2", "hk2-utils", "2.6.1"));
		manager.loadLibrary(buildLibrary("org.javassist", "javassist", "3.22.0-CR2"));
		manager.loadLibrary(buildLibrary("org.glassfish.jersey.media", "jersey-media-json-jackson", "2.29.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.jersey.ext", "jersey-entity-filtering", "2.29.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.jersey.containers", "jersey-container-grizzly2-http", "2.29.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.hk2.external", "jakarta.inject", "2.6.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.grizzly", "grizzly-http-server", "2.4.4"));
		manager.loadLibrary(buildLibrary("org.glassfish.grizzly", "grizzly-http", "2.4.4"));
		manager.loadLibrary(buildLibrary("org.glassfish.grizzly", "grizzly-framework", "2.4.4"));
		manager.loadLibrary(buildLibrary("jakarta.ws.rs", "jakarta.ws.rs-api", "2.1.6"));
		manager.loadLibrary(buildLibrary("org.glassfish.jersey.core", "jersey-server", "2.29.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.jersey.core", "jersey-client", "2.29.1"));
		manager.loadLibrary(buildLibrary("org.glassfish.jersey.media", "jersey-media-jaxb", "2.29.1"));
		manager.loadLibrary(buildLibrary("jakarta.annotation", "jakarta.annotation-api", "1.3.5"));
		manager.loadLibrary(buildLibrary("jakarta.validation", "jakarta.validation-api", "2.0.2"));
		manager.loadLibrary(buildLibrary("org.json", "json", "20180813"));
		manager.loadLibrary(buildLibrary("org.apache.httpcomponents", "httpclient", "4.5.10"));
		manager.loadLibrary(buildLibrary("org.apache.httpcomponents", "httpcore", "4.4.12"));
		manager.loadLibrary(buildLibrary("commons-logging", "commons-logging", "1.2"));
		manager.loadLibrary(buildLibrary("commons-codec", "commons-codec", "1.11"));
		manager.loadLibrary(buildLibrary("org.apache.httpcomponents", "httpmime", "4.5.10"));
		manager.loadLibrary(buildLibrary("commons-io", "commons-io", "2.6"));
		manager.loadLibrary(buildLibrary("org.slf4j", "slf4j-api", "1.7.29"));
		
		manager.loadLibrary(buildLibrary("org.telegram", "telegrambotsextensions", "4.5"));
		
		manager.loadLibrary(buildLibrary("org.telegram", "telegrambots-abilities", "4.5"));
		manager.loadLibrary(buildLibrary("org.apache.commons", "commons-lang3", "3.9"));
		manager.loadLibrary(buildLibrary("org.mapdb", "mapdb", "3.0.7"));
		manager.loadLibrary(buildLibrary("org.jetbrains.kotlin", "kotlin-stdlib", "1.2.71"));
		manager.loadLibrary(buildLibrary("org.jetbrains.kotlin", "kotlin-stdlib-common", "1.2.71"));
		manager.loadLibrary(buildLibrary("org.jetbrains", "annotations", "13.0"));
		manager.loadLibrary(buildLibrary("org.eclipse.collections", "eclipse-collections-api", "10.1.0"));
		manager.loadLibrary(buildLibrary("org.eclipse.collections", "eclipse-collections", "10.1.0"));
		manager.loadLibrary(buildLibrary("org.eclipse.collections", "eclipse-collections-forkjoin", "10.1.0"));
		manager.loadLibrary(buildLibrary("net.jpountz.lz4", "lz4", "1.3.0"));
		manager.loadLibrary(buildLibrary("org.mapdb", "elsa", "3.0.0-M5"));
		
		manager.loadLibrary(buildLibrary("org.telegram", "telegrambots-meta", "4.5"));
		manager.loadLibrary(buildLibrary("com.google.inject", "guice", "4.2.2"));
		manager.loadLibrary(buildLibrary("javax.inject", "javax.inject", "1"));
		manager.loadLibrary(buildLibrary("aopalliance", "aopalliance", "1.0"));
		manager.loadLibrary(buildLibrary("com.google.guava", "guava", "28.1-jre"));
		manager.loadLibrary(buildLibrary("com.google.guava", "failureaccess", "1.0.1"));
		manager.loadLibrary(buildLibrary("com.google.guava", "listenablefuture", "9999.0-empty-to-avoid-conflict-with-guava"));
		manager.loadLibrary(buildLibrary("com.google.code.findbugs", "jsr305", "3.0.2"));
		manager.loadLibrary(buildLibrary("org.checkerframework", "checker-qual", "2.8.1"));
		manager.loadLibrary(buildLibrary("com.google.errorprone", "error_prone_annotations", "2.3.2"));
		manager.loadLibrary(buildLibrary("com.google.j2objc", "j2objc-annotations", "1.3"));
		manager.loadLibrary(buildLibrary("org.codehaus.mojo", "animal-sniffer-annotations", "1.18"));
	}
	
	private static Library buildLibrary(String groupId, String artifactId, String version) {
		return Library.builder().groupId(groupId).artifactId(artifactId).version(version).build();
	}
}
