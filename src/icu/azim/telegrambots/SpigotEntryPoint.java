package icu.azim.telegrambots;

import org.bukkit.plugin.java.JavaPlugin;

import net.byteflux.libby.BukkitLibraryManager;

public class SpigotEntryPoint extends JavaPlugin{
	@Override
	public void onLoad() {
		TelegramBotsAPIPlugin.loadLibs(new BukkitLibraryManager(this));
	}
}
