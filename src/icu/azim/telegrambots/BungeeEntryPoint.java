package icu.azim.telegrambots;
import net.byteflux.libby.BungeeLibraryManager;
import net.md_5.bungee.api.plugin.Plugin;

public class BungeeEntryPoint extends Plugin{
	@Override
	public void onLoad() {
		TelegramBotsAPIPlugin.loadLibs(new BungeeLibraryManager(this));
	}
}
